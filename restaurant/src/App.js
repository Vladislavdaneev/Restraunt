import Authorization from "./Components/Authorization/Authorization";
import Store from "./Components/Store/Store";
import Cart from "./Components/Cart/Cart"
import {Routes, Route} from "react-router-dom";
import './App.css'
function App() {

  return (

        <div className="App">
                <Routes>
                    <Route path="/" element={<Authorization/>}/>
                    <Route path="store" element={<Store/>}/>
                    <Route path="store/cart" element={<Cart/>}/>
                </Routes>
        </div>

  );
}

export default App;
