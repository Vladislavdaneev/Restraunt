import {createSlice} from "@reduxjs/toolkit";


const toolkitSlice = createSlice({
    name:"toolkit",
    initialState: {
        count: 0,
        totalCost: 0,
        arrayOfItemNames:[
        ]
    },
    reducers:{
        addItemName: function (state, action) {
            if (state.arrayOfItemNames.every(element=>element.id!==action.payload.id)){
                state.arrayOfItemNames.push(action.payload)
            } else {
                state.arrayOfItemNames[state.arrayOfItemNames.findIndex(element=>element.id===action.payload.id
                )].purchasedNumber++
            }
            state.totalCost += Number(action.payload.cost)
            state.count+=1
        },
        removeItemName: function (state,action){
            const getIndexOfElement = state.arrayOfItemNames.findIndex(element=>element.id===action.payload)
            console.log("элемент удален")
            return {
                count: state.count-state.arrayOfItemNames[getIndexOfElement].purchasedNumber,
                totalCost: state.totalCost-state.arrayOfItemNames[getIndexOfElement].cost*state.arrayOfItemNames[getIndexOfElement].purchasedNumber,
                arrayOfItemNames:state.arrayOfItemNames.filter(element=>element.id!==action.payload)
            }
        }
    }
})
export default toolkitSlice.reducer
export const {addItemName, removeItemName} = toolkitSlice.actions
