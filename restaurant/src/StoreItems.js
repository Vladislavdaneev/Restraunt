import item1 from '../src/images/StoreItem1.png'
import item2 from '../src/images/StoreItem2.png'
import item3 from '../src/images/StoreItem3.png'


export const content = [
  {
    id: '1',
    name: "Устрицы по рокфеллеровски",
    cost: "2700",
    description: "Значимость этих проблем настолько очевидна, что укрепление и развитие структуры ",
    amount: "/ 500 г.",
    picture:item1,
    purchasedNumber:1
  },
  {
    id: '2',
    name: "Свиные ребрышки на гриле с зеленью",
    cost: "1600",
    description: "Не следует, однако забывать, что реализация намеченных плановых",
    amount: "/ 750 г.",
    picture:item2,
    purchasedNumber:1
  },
  {
    id: '3',
    name: "Креветки по-королевски в лимонном соке",
    cost: "1820",
    description: "Значимость этих проблем настолько очевидна, что укрепление и развитие структуры ",
    amount: "/ 7 шт.",
    picture:item3,
    purchasedNumber:1
  },
  {
    id: '4',
    name: "Устрицы по рокфеллеровски",
    cost: "2700",
    description: "Не следует, однако забывать, что реализация намеченных плановых",
    amount: "/ 500 г.",
    picture:item1,
    purchasedNumber:1
  },
  {
    id: '5',
    name: "Устрицы по рокфеллеровски",
    cost: "2700",
    description: "Значимость этих проблем настолько очевидна, что укрепление и развитие структуры ",
    amount: "/ 500 г.",
    picture:item1,
    purchasedNumber:1
  },
  {
    id: '6',
    name: "Свиные ребрышки на гриле с зеленью",
    cost: "1600",
    description: "Не следует, однако забывать, что реализация намеченных плановых",
    amount: "/ 750 г.",
    picture:item2,
    purchasedNumber:1
  },
  {
    id: '7',
    name: "Креветки по-королевски в лимонном соке",
    cost: "1820",
    description: "Значимость этих проблем настолько очевидна, что укрепление и развитие структуры ",
    amount: "/ 7 шт.",
    picture:item3,
    purchasedNumber:1
  },
  {
    id: '8',
    name: "Устрицы по рокфеллеровски",
    cost: "2700",
    description: "Не следует, однако забывать, что реализация намеченных плановых",
    amount: "/ 500 г.",
    picture:item1,
    purchasedNumber:1
  }
]
