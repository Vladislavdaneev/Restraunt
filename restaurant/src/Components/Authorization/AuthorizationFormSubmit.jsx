import React from "react";
import { useNavigate } from "react-router-dom";
import styles from "./AuthorizationStyles/AuthorizationFormSubmit.module.css"

function AuthorizationFormSubmit (props){
    localStorage.setItem('login','user')
    localStorage.setItem('password','user')
    let navigate = useNavigate()
    function checkLogin(login,password) {
        if (login===localStorage.getItem('login')&&(password===localStorage.getItem('password'))){
            navigate("/store");
        } else {
            console.log('введите правильные логин и пароль')
        }
    }
    return(
        <button type="submit" className={styles.authorizationSubmitAuthorization} onClick={(event) => {
            event.preventDefault()
            checkLogin(props.loginValue,props.passwordValue)

        }}>Войти</button>
    )
}
export default AuthorizationFormSubmit