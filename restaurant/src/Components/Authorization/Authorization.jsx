import React from 'react';
import AuthorizationForm from "./AuthorizationForm";
import styles from './AuthorizationStyles/Authorization.module.css'
function Authorization (){
    return(
        <div className={styles.main}>
            <div className={styles['Authorization-Block']}>
                <div className={styles['Authorization-Title']}>
                    <span>ВХОД</span>
                </div>
                <AuthorizationForm/>


            </div>
        </div>
)

}
export default Authorization