import React, {useState} from "react";
import AuthorizationFormSubmit from "./AuthorizationFormSubmit";
import styles from './AuthorizationStyles/AuthorizationForm.module.css'

function AuthorizationForm(){
    const [passwordValue, SetPassword] = useState('')
    const HandleChangePassword = (event) => {
        SetPassword(event.target.value)
    }
    console.log('password  '+passwordValue)
    const [loginValue, SetLogin] = useState('')
    const HandleChangeLogin = (event) =>{
        SetLogin(event.target.value)
    }

    console.log('login  '+loginValue)
    return(
        <form className={styles['Authorization-form']} method="POST" action="/submit" >
            <div className="Authorization__Login-Block">
                <input className={styles['Login__Input']} type="text" placeholder="Email" value={loginValue} onChange={HandleChangeLogin}/>
            </div>
            <div className="Authorization__Password-Block">
                <input className={styles['Password__Input']} type="password" placeholder="Пароль" value={passwordValue} onChange={HandleChangePassword}/>
            </div>
            <div className={styles['Authorization__Accept-News']}>
                <input type="checkbox" className={styles['Accept-Mark']}/>
                <label htmlFor="checkbox">Я согласен получать обновления на почту</label>
            </div>
            <AuthorizationFormSubmit loginValue={loginValue} passwordValue={passwordValue}/>
        </form>
)
}
export default AuthorizationForm
