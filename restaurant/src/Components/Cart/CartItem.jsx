import React from "react";
import styles from './CartItem.module.css'
import {removeItemName} from "../../redux/toolkitSlice";
import {useDispatch} from "react-redux";
function checkMultipleItems(purchasedNumber){
    if (purchasedNumber>1){
        return purchasedNumber+' шт / '
    } else {
        return ''
    }
}
function CartItem(props){
    const dispatch = useDispatch()

    return(
        <div className={styles['Wrapper']}>
            <div className={styles['Cart-Item__Info-Wrapper']}>
                <img className={styles['Cart-Item__picture']} src={props.picture} alt='food'/>
                <div className={styles['Cart-Item__Name-Wrapper']}>
                    <span className={styles['Cart-Item__name']}>{props.name}</span>

                </div>
            </div>
            <div className={styles['Cart-Item__Action-Wrapper']}>
                <span className={styles['Cart-Item__amount']}>{checkMultipleItems(props.purchasedNumber)}</span>
                <span className={styles['Cart-Item__cost']}>{props.cost*props.purchasedNumber}</span>
                <button className={styles['Cart-Item__remove']} onClick={() => (dispatch(removeItemName(props.id)))}>X</button>
            </div>
        </div>
    )
}
export default CartItem