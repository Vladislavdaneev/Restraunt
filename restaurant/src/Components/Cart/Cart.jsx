import React from "react";
import styles from './Cart.module.css'
import CartItem from "./CartItem";
import {useSelector} from "react-redux";

function Cart(){
    const arrayOfItemNames = useSelector(state=>state.toolkit.arrayOfItemNames)
    const totalCost = useSelector(state=> state.toolkit.totalCost)
    return(
        <div className={styles.Cart}>
            <header className={styles['Cart-Header']}>
                <h2 className={styles['Header-Title']}>Корзина с выбранными товарами</h2>
            </header>
            <main className={styles['Cart-Main']}>
                {
                    arrayOfItemNames.map((Item,index)=>{
                        return(
                            <CartItem
                            key={index}
                            id={Item.id}
                            name={Item.name}
                            picture={Item.picture}
                            cost={Item.cost}
                            purchasedNumber={Item.purchasedNumber}
                            />
                        )
                    })
                }
            </main>
            <footer className={styles['Cart-Footer']}>
                <div className={styles['Cart-Footer_Wrapper']}>
                    <span className={styles['Cart-Footer__Total']}>заказ на сумму: </span>
                    <span className={styles['Cart-Footer__Amount']}>{totalCost} ₽</span>
                </div>

                <button className={styles['Cart-Footer_Pay']}>Оформить заказ</button>
            </footer>
        </div>
    )
}
export default Cart