import React from "react";
import styles from "./StoreItem.module.css"
import { useDispatch } from 'react-redux'
import {addItemName} from '../../redux/toolkitSlice'



function StoreItem (props){
    const dispatch = useDispatch()
    return(
        <div className={styles['Store-Item']}>
            <img src={props.picture} alt="Item"/>
            <div className={styles['Item__Information']}>
                <div className={styles.Wrapper}>
                    <h3 className={styles['Item__Title']}>{props.name}</h3>
                    <p className={styles['Item__Description']}>{props.description}</p>
                </div>
                <div className={styles['Item__Footer']}>
                    <div className={styles['Item__Cost-Wrapper']}>
                        <span className={styles['Item__Cost']}>{props.cost} ₽</span>
                        <span className={styles['Item__Amount']}>{props.amount}</span>
                    </div>
                    <button className={styles.AddToCart} onClick={() => (dispatch(addItemName(props)))}>+</button>
                </div>
            </div>

        </div>
    )
}
export default StoreItem

