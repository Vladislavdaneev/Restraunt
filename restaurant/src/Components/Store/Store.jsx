import React from "react";
import {content} from "../../StoreItems.js"
import styles from "./Store.module.css"
import StoreItem from "./StoreItem";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import CartIcon from "../../images/CartIcon.png"

function Store (){
    const totalCost = useSelector(state=> state.toolkit.totalCost)
    const count = useSelector(state=> state.toolkit.count)
    return(
        <main className={styles.main}>
            <header className={styles.header}>
                <h2 className={styles['Store__Title']}>наша продукция</h2>
                <Link to={'cart'}>
                    <div className={styles['Cart-Wrapper']}>
                        <div className={styles['Total__Items']}>
                            <span>{count} товара</span>
                            <span>на сумму {totalCost} ₽</span>
                        </div>
                        <div className={styles['Total__Icon']}><img src={CartIcon} alt='Cart Icon'/>   </div>
                    </div>
                </Link>
            </header>
            <div className={styles.Wrapper}>
                {
                    content.map(({amount, cost, description, id, name, picture, purchasedNumber},index)=>{
                        return (
                            <StoreItem
                            key={index}
                            id={id}
                            name={name}
                            cost={cost}
                            description={description}
                            amount={amount}
                            picture={picture}
                            purchasedNumber={purchasedNumber}
                            />
                        )
                })
                }
            </div>
        </main>
    )
}
export default Store